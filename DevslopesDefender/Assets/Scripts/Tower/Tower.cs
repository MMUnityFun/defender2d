﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Tower : MonoBehaviour {

	[SerializeField] private float timeBetweenAttacks;
	[SerializeField] private float attackRadius;
	[SerializeField] private Projectile projectile;
	private Enemy targetEnemy = null;
	private float attackCounter;
	private bool isAttacking = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		attackCounter -= Time.deltaTime;
		if(targetEnemy == null || targetEnemy.IsDead) {
			Enemy nearestEnemy = GetNearestEnemyInRange();
			if(nearestEnemy != null && Vector2.Distance(transform.localPosition, nearestEnemy.transform.localPosition) <= attackRadius) {
				targetEnemy = nearestEnemy;
			}
		} else {
			if(attackCounter <= 0.0f) {
				isAttacking = true;
				attackCounter = timeBetweenAttacks;
			} else { 
				isAttacking = false;
			}

			if(Vector2.Distance(transform.localPosition, targetEnemy.transform.localPosition) > attackRadius) {
				targetEnemy = null;
			}
		}
	}

	void FixedUpdate() {
		if(isAttacking) {
			Attack();
		}
	}

	public void Attack() {
		isAttacking = false;
		Projectile newProjectile = Instantiate(projectile) as Projectile;
		newProjectile.transform.localPosition = transform.localPosition;

		if(newProjectile.ProjectileType == proType.arrow) {
			GameManager.Instance.AudioSource.PlayOneShot(SoundManager.Instance.Arrow);
		} else if (newProjectile.ProjectileType == proType.fireball) {
			GameManager.Instance.AudioSource.PlayOneShot(SoundManager.Instance.Fireball);
		} else if (newProjectile.ProjectileType == proType.rock){
			GameManager.Instance.AudioSource.PlayOneShot(SoundManager.Instance.Rock);
		}

		if(targetEnemy == null) {
			Destroy(newProjectile);
		} else {
			//
			StartCoroutine(MoveProjectile(newProjectile));
		}
	}

	IEnumerator MoveProjectile(Projectile projectile) {
		while(getTargetDistance(targetEnemy) > 0.20f && projectile != null && targetEnemy != null) {
			Vector2 dir = targetEnemy.transform.localPosition - transform.localPosition;
			float angleDirection = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

			projectile.transform.rotation = Quaternion.AngleAxis(angleDirection, Vector3.forward);
			projectile.transform.localPosition = Vector2.MoveTowards(projectile.transform.localPosition, targetEnemy.transform.localPosition, 5.0f * Time.deltaTime);

			yield return null;
		}

		if(projectile != null || targetEnemy == null) {
			Destroy(projectile.gameObject); 
		}
	}

	private float getTargetDistance(Enemy enemy1) {
		if(enemy1 == null) {
			enemy1 = GetNearestEnemyInRange();
			if(enemy1 == null) {
				return 0.0f;
			}
		}

		return Mathf.Abs(Vector2.Distance(transform.localPosition, enemy1.transform.localPosition));
	}

	private List<Enemy> GetEnemiesInRange() {
		List<Enemy> enemiesInRange = new List<Enemy>();

		foreach(Enemy enemy in GameManager.Instance.EnemyList) {
			if(Vector2.Distance(transform.position, enemy.transform.localPosition) <= attackRadius) {
				enemiesInRange.Add(enemy);
			}
		}

		return enemiesInRange;
	}

	private Enemy GetNearestEnemyInRange() {
		Enemy nearestEnemy = null;
		float smallestDistance = float.PositiveInfinity;

		foreach(Enemy enemy in GetEnemiesInRange()) {
			if(Vector2.Distance(transform.localPosition, enemy.transform.localPosition) < smallestDistance) {
				nearestEnemy = enemy;
				smallestDistance = Vector2.Distance(transform.localPosition, enemy.transform.localPosition);
			}
		}

		return nearestEnemy;
	}
}
